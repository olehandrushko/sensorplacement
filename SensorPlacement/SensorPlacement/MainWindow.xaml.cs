﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Navigation;
using System.Windows.Shapes;
using SensorPlacement.Charting;

namespace SensorPlacement
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        // transform class object for rotate the 3d model
        public TransformMatrix m_transformMatrix = new TransformMatrix();

        // ***************************** 3d chart ***************************
        private Chart3D m_3dChart;       // data for 3d chart
        public int m_nChartModelIndex = -1;         // model index in the Viewport3d
        public int m_nSurfaceChartGridNo = 100;     // surface chart grid no. in each axis
        public int m_nScatterPlotDataNo = 5000;     // total data number of the scatter plot

        // ***************************** selection rect ***************************
        ViewportRect m_selectRect = new ViewportRect();
        public int m_nRectModelIndex = -1;

        public MainWindow()
        {
            InitializeComponent();
        }


        public void TestScatterPlot(int nDotNo)
        {
            // 1. set scatter chart data no.
            m_3dChart = new ScatterChart3D();
            m_3dChart.SetDataNo(nDotNo);

            // 2. set property of each dot (size, position, shape, color)
            Random randomObject = new Random();
            int nDataRange = 200;
            nDotNo = 5;
            var delta = 30;

            int s = 0;
            for (int i = 0; i < nDotNo; i++)
            {
                for (int j = 0; j < nDotNo; j++)
                {
                    for (int k = 0; k < nDotNo; k++)
                    {
                        
                        ScatterPlotItem plotItem = new ScatterPlotItem();

                        plotItem.w = 10;
                        plotItem.h = 10;


                        plotItem.x = i * (10 + delta);
                        plotItem.y = j * (10 + delta);
                        plotItem.z = k * (10 + delta);

                        plotItem.shape = randomObject.Next(4);

                        Byte nR = (Byte)randomObject.Next(256);
                        Byte nG = (Byte)randomObject.Next(256);
                        Byte nB = (Byte)randomObject.Next(256);

                        plotItem.color = Color.FromRgb(nR, nG, nB);
                        ((ScatterChart3D)m_3dChart).SetVertex(s, plotItem);
                        s++;
                    }
                }
            }

            // 3. set the axes
            m_3dChart.GetDataRange();
            m_3dChart.SetAxes();

            // 4. get Mesh3D array from the scatter plot
            ArrayList meshs = ((ScatterChart3D)m_3dChart).GetMeshes();

            // 5. display model vertex no and triangle no
            UpdateModelSizeInfo(meshs);

            // 6. display scatter plot in Viewport3D
            var model3d = new SensorPlacement.Charting.Model3D();
            m_nChartModelIndex = model3d.UpdateModel(meshs, null, m_nChartModelIndex, this.mainViewport);

            // 7. set projection matrix
            float viewRange = (float)nDataRange;
            m_transformMatrix.CalculateProjectionMatrix(0, viewRange, 0, viewRange, 0, viewRange, 0.5);
            TransformChart();
        }
        private void TransformChart()
        {
            if (m_nChartModelIndex == -1) return;
            ModelVisual3D visual3d = (ModelVisual3D)(this.mainViewport.Children[m_nChartModelIndex]);
            if (visual3d.Content == null) return;
            Transform3DGroup group1 = visual3d.Content.Transform as Transform3DGroup;
            group1.Children.Clear();
            group1.Children.Add(new MatrixTransform3D(m_transformMatrix.m_totalMatrix));
        }
        private void UpdateModelSizeInfo(ArrayList meshs)
        {
            int nMeshNo = meshs.Count;
            int nChartVertNo = 0;
            int nChartTriangelNo = 0;
            for (int i = 0; i < nMeshNo; i++)
            {
                nChartVertNo += ((Mesh3D)meshs[i]).GetVertexNo();
                nChartTriangelNo += ((Mesh3D)meshs[i]).GetTriangleNo();
            }
        }
        public void OnViewportMouseDown(object sender, MouseButtonEventArgs args)
        {
            Point pt = args.GetPosition(mainViewport);
            if (args.ChangedButton == MouseButton.Left)         // rotate or drag 3d model
            {
                m_transformMatrix.OnLBtnDown(pt);
            }
            else if (args.ChangedButton == MouseButton.Right)   // select rect
            {
                m_selectRect.OnMouseDown(pt, mainViewport, m_nRectModelIndex);
            }
        }

        public void OnViewportMouseMove(object sender, MouseEventArgs args)
        {
            Point pt = args.GetPosition(mainViewport);

            if (args.LeftButton == MouseButtonState.Pressed)                // rotate or drag 3d model
            {
                m_transformMatrix.OnMouseMove(pt, mainViewport);

                TransformChart();
            }
            else if (args.RightButton == MouseButtonState.Pressed)          // select rect
            {
                m_selectRect.OnMouseMove(pt, mainViewport, m_nRectModelIndex);
            }
            else
            {

                //String s1;
                //Point pt2 = m_transformMatrix.VertexToScreenPt(new Point3D(0.5, 0.5, 0.3), mainViewport);
                //s1 = string.Format("Screen:({0:d},{1:d}), Predicated: ({2:d}, H:{3:d})",
                //    (int)pt.X, (int)pt.Y, (int)pt2.X, (int)pt2.Y);
                //this.statusPane.Text = s1;

            }
        }

        public void OnKeyDown(object sender, KeyEventArgs args)
        {
            m_transformMatrix.OnKeyDown(args);
            TransformChart();
        }

        public void OnMouseWheel(object sender, MouseWheelEventArgs args)
        {
            m_transformMatrix.OnMouseWheel(args);
            TransformChart();

        }

        public void OnViewportMouseUp(object sender, System.Windows.Input.MouseButtonEventArgs args)
        {
            Point pt = args.GetPosition(mainViewport);
            if (args.ChangedButton == MouseButton.Left)
            {
                m_transformMatrix.OnLBtnUp();
            }
            else if (args.ChangedButton == MouseButton.Right)
            {
                if (m_nChartModelIndex == -1) return;
                // 1. get the mesh structure related to the selection rect
                MeshGeometry3D meshGeometry = SensorPlacement.Charting.Model3D.GetGeometry(mainViewport, m_nChartModelIndex);
                if (meshGeometry == null) return;

                // 2. set selection in 3d chart
                m_3dChart.Select(m_selectRect, m_transformMatrix, mainViewport);

                // 3. update selection display
                m_3dChart.HighlightSelection(meshGeometry, Color.FromRgb(200, 200, 200));
            }
        }

        private void surfaceButton_Click(object sender, RoutedEventArgs e)
        {
            int model3d_X = Int32.Parse(txt_len_X.Text);
            int model3d_Y = Int32.Parse(txt_len_Y.Text);
            int model3d_Z = Int32.Parse(txt_len_Z.Text);
            int diff = Int32.Parse(txt_difference.Text);

            int amountOnX = model3d_X / diff;
            int amountOnY = model3d_Y / diff;
            int amountOnZ = model3d_Z / diff;

            int amountAll = amountOnX * amountOnY * amountOnZ;

            labelVertNo.Content = "Кількість датчиків: " + amountAll;

            //m_transformMatrix.ResetView();
            TestScatterPlot(amountAll, amountOnX, amountOnY, amountOnZ, diff);
            TransformChart();

        }

        public void TestScatterPlot(int nDotNo, int ax, int ay, int az, int diff)
        {
            // 1. set scatter chart data no.
            m_3dChart = new ScatterChart3D();
            m_3dChart.SetDataNo(nDotNo);

            // 2. set property of each dot (size, position, shape, color)
            Random randomObject = new Random();
            int nDataRange = 200;

            var delta = diff;

            int s = 0;
            for (int i = 0; i < ax; i++)
            {
                for (int j = 0; j < ay; j++)
                {
                    for (int k = 0; k < az; k++)
                    {

                        ScatterPlotItem plotItem = new ScatterPlotItem();

                        plotItem.w = 10;
                        plotItem.h = 10;


                        plotItem.x = i * (10 + delta);
                        plotItem.y = j * (10 + delta);
                        plotItem.z = k * (10 + delta);

                        plotItem.shape = randomObject.Next(4);

                        Byte nR = (Byte)randomObject.Next(256);
                        Byte nG = (Byte)randomObject.Next(256);
                        Byte nB = (Byte)randomObject.Next(256);

                        plotItem.color = Color.FromRgb(nR, nG, nB);
                        ((ScatterChart3D)m_3dChart).SetVertex(s, plotItem);
                        s++;
                    }
                }
            }

            // 3. set the axes
            m_3dChart.GetDataRange();
            m_3dChart.SetAxes();

            // 4. get Mesh3D array from the scatter plot
            ArrayList meshs = ((ScatterChart3D)m_3dChart).GetMeshes();

            // 5. display model vertex no and triangle no
            UpdateModelSizeInfo(meshs);

            // 6. display scatter plot in Viewport3D
            var model3d = new SensorPlacement.Charting.Model3D();
            m_nChartModelIndex = model3d.UpdateModel(meshs, null, m_nChartModelIndex, this.mainViewport);

            // 7. set projection matrix
            float viewRange = (float)nDataRange;
            m_transformMatrix.CalculateProjectionMatrix(0, viewRange, 0, viewRange, 0, viewRange, 0.5);
            TransformChart();
        }
    }
}
